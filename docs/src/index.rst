.. skeleton documentation master file, created by
   sphinx-quickstart on Thu May 17 15:17:35 2018.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.


.. HOME SECTION ==================================================

.. Hidden toctree to manage the sidebar navigation.


SDP DAL Schemas
===============

This project contains a list of schemas
used for transferring data and metadata
using the :doc:`sdp-dal-prototype:index` code.

.. toctree::
  :maxdepth: 2

  api
  CHANGELOG
