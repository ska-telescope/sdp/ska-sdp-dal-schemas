Change Log
###########

Development
-----------

Changed
^^^^^^^

* Poetry housekeeping, updated to ``>=1.8.2``.

0.6.1
-----

Added
^^^^^

* New ``start_scan`` and ``end_scan`` procedures,
  each taking a ``scan_id`` as their own argument.
  Will be used to inform processors
  about scans starting/ending
  in SDP.

Changed
^^^^^^^

* Poetry housekeeping, updated to ``>=1.6.1``.

0.6.0
-----

Changed
^^^^^^^

* **BREAKING** Updated minimum required python version to 3.9
* Removed internal usage of ``pkg_resources`` API,
  using ``importlib.resources`` instead.

0.5.0
-----

Added
^^^^^

* New ``process_visibility`` procedure,
  modelled after the existing ``read_payload`` one,
  but able to transmit the full contents
  of a ``Visibility`` dataset,
  including data for multiple time values.
* Column tables in table definitions
  can now be lists of fixed size.
* New ``ANTENNA2`` table schema
  that better matches the ``Configuration`` class
  from the ``ska-sdp-datamodel`` package.

Changed
^^^^^^^

* Add dimension information
  to tensors in ``read_payload`` procedure.
  The dimension names align with the ``Visibility`` class
  from the ``ska-sdp-datamodel`` package.

0.4.0.post0
-----------

Changed
^^^^^^^

* Ported package to poetry and standard CI tooling.

0.4.0
-----

* Fixed declared dimensions order for visibility tensor.
* Adjust weights and flags dimensions and type
  to match expectations from downstream pipeline components.

0.3.2
-----

* Adjusted tables to latest ADR-54 schema:

  * Renamed SpectralWindow's ``name`` -> ``spectral_window_id``
  * Beam ID type changed from ``int`` -> ``string``
  * Renamed ``phase_dir``'s ``time_origin`` -> ``reference_time``, added
    ``reference_frame``.

0.3.1
-----

* Add name to spectral window

0.3.0
-----

* Add scan_id to the procedure
* Updated tables to scan model

0.2.3
-----

* Updated the sdp-dal dependency

0.2.2
-----

* Packaged for release

0.2.1
-----

* Added the extra-requirements for publication in the CAR

0.2
---

* Added the flags, weights and sigma to the procedure
* Updated the procedure schema to include channel_ids- this is an array of ints listing the channel numbers of those included in this payload.
  -- Steve Ord 

0.1
---

* Initial, simple list of table and procedure schemas,
  along with python code to load them as Python objects.
