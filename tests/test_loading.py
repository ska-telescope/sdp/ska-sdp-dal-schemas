"""Tests for built-in schema loading"""
import sdp_dal_schemas


def test_default_schemas_exist():
    """Check that TABLES and PROCEDURES is available"""
    assert sdp_dal_schemas.TABLES is not None
    assert sdp_dal_schemas.PROCEDURES is not None
    assert "read_payload" in sdp_dal_schemas.PROCEDURES
    assert "process_visibility" in sdp_dal_schemas.PROCEDURES
    assert "start_scan" in sdp_dal_schemas.PROCEDURES
    assert "end_scan" in sdp_dal_schemas.PROCEDURES
