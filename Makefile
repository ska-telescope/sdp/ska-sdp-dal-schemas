# Use bash shell with pipefail option enabled so that the return status of a
# piped command is the value of the last (rightmost) commnand to exit with a
# non-zero status. This lets us pipe output into tee but still exit on test
# failures.
SHELL = /bin/bash
.SHELLFLAGS = -o pipefail -c

include .make/base.mk
include .make/python.mk

DOCS_SPHINXOPTS = -W --keep-going

install-doc-requirements-with-poetry:
	poetry config virtualenvs.create $(POETRY_CONFIG_VIRTUALENVS_CREATE)
	poetry install --only main,docs

docs-pre-build: install-doc-requirements-with-poetry
