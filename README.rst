SKA SDP DAL Schemas
===================

This python package provides schemas for exchanging data
using the ``sdp-dal-prototype``.

Installation
------------

Installation instructions can be found
in the `online documentation <https://developer.skatelescope.org/projects/ska-sdp-dal-schemas/en/latest/?badge=latest>`_.
