"""SDP DAL schemas"""
from importlib.metadata import version

from ._loader import PROCEDURES, TABLES

__version__ = version("ska-sdp-dal-schemas")
