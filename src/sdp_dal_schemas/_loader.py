"""Automatic loading of built-in schemas"""
import json
from importlib.resources import files

from .common import load_procedure_schemas, load_table_schemas


def _json_contents(resource):
    resource_file = files(__package__).joinpath(resource)
    return json.loads(resource_file.read_bytes())


def _load_default_schemas():
    tables = load_table_schemas(_json_contents("schemas/tables.json"))
    procedures = load_procedure_schemas(
        _json_contents("schemas/procedures.json"), tables
    )
    return tables, procedures


TABLES, PROCEDURES = _load_default_schemas()
